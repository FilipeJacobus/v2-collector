package collector

import (
	"time"

	"github.com/go-pg/pg"
	log "github.com/sirupsen/logrus"
)

func GetPostgreDB() *pg.DB {
	//"postgres://dev:deV*G0br4X@54.233.76.15:5432/GobraxDB_DEV"
	c := GetConf()

	timeout := 60
	urlOptions, err := pg.ParseURL(c.DataBase.Access)
	if err != nil {
		log.Error(err)
		return nil
	}

	db := pg.Connect(urlOptions)

	_, err = db.Exec("SELECT 1")
	if err != nil {
		log.Error(err)
		return nil
	}

	if timeout > 0 {
		db = db.WithTimeout(time.Second * time.Duration(timeout))
	}

	return db
}

func GetLeverageData(startDate time.Time, endDate time.Time) (LeverageData []Storage, err error) {

	db := GetPostgreDB()
	defer db.Close()

	// TODO: add virtual odometer and acceleration pedal
	sql := `select id, unit_type, unit_id,	time, lat, lon,	speed,	course,	height,	sats, params, object_id from storage0 where (time > ? and time < ?)`

	_, err = db.Query(&LeverageData, sql, makeTimestamp(startDate), makeTimestamp(endDate))
	if err != nil {
		return nil, err
	}

	return LeverageData, nil
}

func GetTripTelemetryProfile(vehicleID int) (tripTelemetryProfile, error) {

	db := GetPostgreDB()
	defer db.Close()

	var tripTelemetryProfile tripTelemetryProfile

	sql := `select vehicleid as vehicle_id, 
	maxspeed as max_speed, 
	gpsorcan as distance_method,
	overspeedleverage as leverage_speed_peak, 
	usevirtodometer as use_virtual_odometer, 
	neutralrangeend as neutral_range_end, 
	whiterangeend as white_range_end,
	extraeconomyrangestart as extra_economy_range_start, 
	extraeconomyrangeend as extra_economy_range_end, 
	powerrangestart as power_range_start, 
	hasecoroll as has_eco_roll, 
	hasdafecoroll as has_daf_eco_roll 
	from vehicleprofile where deletedat<'2000-01-01 00:00:00' and vehicleid=?`
	_, err := db.QueryOne(&tripTelemetryProfile, sql, vehicleID)
	if err != nil {
		return tripTelemetryProfile, err
	}

	return tripTelemetryProfile, nil
}

func makeTimestamp(t time.Time) int64 {
	return t.UnixNano() / 1e6 / 1000
}
