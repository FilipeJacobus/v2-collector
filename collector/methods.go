package collector

import (
	"encoding/json"
	"io/ioutil"
	"strconv"
	"time"
	"v2-collector/pb"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

func Collect() {
	c := GetConf()

	end := time.Now()
	d := time.Duration(c.Collect.TimeTick * (1000000000 * 60))

	init := end.Add(d * -1)

	getV2Data(init, end)

}

func getV2Data(startDate, endDate time.Time) {
	var pbf pb.AddRequest

	leverageData, _ := GetLeverageData(startDate, endDate)

	for _, ld := range leverageData {

		switch ld.UnitType {
		case "omnicomm":
			pbf = OmniToPb(ld)

		case "ruptella":
			pbf = RuptToPb(ld)

		}

		sendToTelemetry(pbf)
	}
}

func OmniToPb(s Storage) pb.AddRequest {

	var res OmnicommData
	var pbf pb.AddRequest
	json.Unmarshal([]byte(s.Params), &res)

	tm := time.Unix(int64(s.Time), 0)
	ctm := tm.Format("2006-01-02 15:04:05")

	//header
	pbf.Imei = strconv.Itoa(s.ObjectId)
	pbf.DeviceId = strconv.Itoa(s.ObjectId)
	pbf.Protocol = s.UnitType
	pbf.Time = ctm
	pbf.Latitude = s.Lat
	pbf.Longitude = s.Lon
	pbf.GpsSpeed = int32(s.Speed)
	pbf.GpsCourse = int32(s.Course)
	pbf.Height = int32(s.Height)
	pbf.SatelliteQuatity = int32(s.Sats)

	//data
	pbf.IgnitionKey = int32(res.IgnitionKey)
	pbf.BateryLife = int32(res.BateryLife)
	pbf.GsmStatus = int32(res.GsmStatus)
	pbf.GpsStatus = int32(res.GpsStatus)
	pbf.Voltage = int32(res.Voltage) //ver esse
	pbf.Mileage = res.Mileage * 0.001
	pbf.MileageVirt = res.MileageVirt * 0.001
	pbf.Acceleration = float64(res.Acceleration)
	//pbf.Input_0 = res.
	//pbf.Input_1 = res.
	//pbf.Input_2 = res.
	//pbf.Input_3 = res.
	//pbf.Input_4 = res.
	pbf.Rpm = float64(res.Rpm)
	pbf.Lls1Value = int32(res.Lls1Value)
	pbf.Lls1Status = int32(res.Lls1Status)
	pbf.Lls1Temp = int32(res.Lls1Temp)
	pbf.Lls2Value = int32(res.Lls2Value)
	pbf.Lls2Status = int32(res.Lls2Status)
	pbf.Lls2Temp = int32(res.Lls2Temp)
	pbf.Lls3Value = int32(res.Lls3Value)
	pbf.Lls3Status = int32(res.Lls3Status)
	pbf.Lls3Temp = int32(res.Lls3Temp)
	pbf.Lls4Value = int32(res.Lls4Value)
	pbf.Lls4Status = int32(res.Lls4Status)
	pbf.Lls4Temp = int32(res.Lls4Temp)
	pbf.Lls5Value = int32(res.Lls5Value)
	pbf.Lls5Status = int32(res.Lls5Status)
	pbf.Lls5Temp = int32(res.Lls5Temp)
	pbf.Lls6Value = int32(res.Lls6Value)
	pbf.Lls6Status = int32(res.Lls6Status)
	pbf.Lls6Temp = int32(res.lls6_temp)
	pbf.CanFuelLevel = 0
	pbf.ParkingBrakeStatus = int32(res.ParkingBrakeStatus)
	pbf.AccelerationPedalPosition = int32(res.AccelerationPedalPosition) //ver esse
	pbf.EngineOilPressure = int32(res.EngineOilPressure)
	pbf.CoolantTemperature = int32(res.CoolantTemperature)
	pbf.FuelTemperature = int32(res.FuelTemperature)
	pbf.EngineOilTemperature = int32(res.EngineOilTemperature)
	pbf.DailyFuelConsumption = int32(res.DailyFuelConsumption)
	pbf.EventOfInstantaneousFuelEconomy = int32(res.EventOfInstantaneousFuelEconomy)
	pbf.DailyMileage = int32(res.DailyMileage) //ver esse
	pbf.TotalMileage = float64(res.TotalMileage) * 0.125
	pbf.TotalMileageFilled = float64(res.TotalMileageFilled) * 0.125
	pbf.TotalTimeOfEngineOperation = float64(res.TotalTimeOfEngineOperation)
	pbf.TotalFuelConsumption = float64(res.TotalFuelConsumption) * 0.5
	pbf.TotalFuelConsumptionHighResolution = 0
	pbf.ServiceBrakePedalPosition = float64(res.ServiceBrakePedalPosition) * 0.39156863
	pbf.ClutchPedalPosition = int32(res.ClutchPedalPosition) //ver esse
	pbf.CruiseControlStatus = int32(res.CruiseControlStatus)
	pbf.AxlePressure = int32(res.AxlePressure)
	pbf.AxleLoad_1 = 0
	pbf.AxleLoad_2 = 0
	pbf.AxleLoad_3 = 0
	pbf.AxleLoad_4 = 0
	pbf.AxleLoad_5 = 0
	pbf.Adblue = 0
	pbf.Gear = 0
	pbf.ServiceBrakePedalStatus = int32(res.ServiceBrakePedalStatus)
	pbf.ClutchPedalStatus = int32(res.ClutchPedalStatus)
	pbf.MileageUntilTheNextMaintenance = int32(res.MileageUntilTheNextMaintenance)
	pbf.EngineRunTimeUntilTheNextMaintenance = int32(res.EngineRunTimeUntilTheNextMaintenance)
	pbf.AxleIndex = int32(res.AxleIndex)
	pbf.CanSpeed = int32(res.CanSpeed) //ver esse
	pbf.DoorStatus = int32(res.DoorStatus)
	pbf.SeatbeltStatus = int32(res.SeatbeltStatus)
	pbf.DeviceOpen = int32(res.DeviceOpen)
	pbf.OutputStatus = int32(res.OutputStatus)
	pbf.Gpstime = int32(s.Time)
	pbf.IsValid = isValidDate(tm)
	pbf.Id = strconv.Itoa(s.Id)

	return pbf
}

func RuptToPb(s Storage) pb.AddRequest {

	var res RuptellaData
	var pbf pb.AddRequest
	json.Unmarshal([]byte(s.Params), &res)

	tm := time.Unix(int64(s.Time), 0)
	ctm := tm.Format("2006-01-02 15:04:05")

	//header
	pbf.Imei = strconv.Itoa(s.ObjectId)
	pbf.DeviceId = strconv.Itoa(s.ObjectId)
	pbf.Protocol = s.UnitType
	pbf.Time = ctm
	pbf.Latitude = s.Lat
	pbf.Longitude = s.Lon
	pbf.GpsSpeed = int32(s.Speed)
	pbf.GpsCourse = int32(s.Course)
	pbf.Height = int32(s.Height)
	pbf.SatelliteQuatity = int32(s.Sats)

	//data
	pbf.IgnitionKey = int32(res.IgnitionKey)
	pbf.BateryLife = int32(res.BateryLife)
	pbf.GsmStatus = int32(res.GsmStatus)
	pbf.GpsStatus = int32(res.GpsStatus)
	pbf.Voltage = int32(res.Voltage) //ver esse
	pbf.Mileage = res.Mileage * 0.001
	pbf.MileageVirt = res.MileageVirt * 0.001
	pbf.Acceleration = 0
	pbf.Input_0 = 0
	pbf.Input_1 = int32(res.Input1)
	pbf.Input_2 = int32(res.Input2)
	pbf.Input_3 = int32(res.Input3)
	pbf.Input_4 = int32(res.Input4)
	pbf.Rpm = float64(res.Rpm)
	pbf.Lls1Value = int32(res.Lls1Value)
	pbf.Lls1Status = int32(res.Lls1Status)
	pbf.Lls1Temp = int32(res.Lls1Temp)
	pbf.Lls2Value = int32(res.Lls2Value)
	pbf.Lls2Status = int32(res.Lls2Status)
	pbf.Lls2Temp = int32(res.Lls2Temp)
	pbf.Lls3Value = int32(res.Lls3Value)
	pbf.Lls3Status = int32(res.Lls3Status)
	pbf.Lls3Temp = int32(res.Lls3Temp)
	pbf.Lls4Value = int32(res.Lls4Value)
	pbf.Lls4Status = int32(res.Lls4Status)
	pbf.Lls4Temp = int32(res.Lls4Temp)
	pbf.Lls5Value = int32(res.Lls5Value)
	pbf.Lls5Status = int32(res.Lls5Status)
	pbf.Lls5Temp = int32(res.Lls5Temp)
	pbf.Lls6Value = int32(res.Lls6Value)
	pbf.Lls6Status = int32(res.Lls6Status)
	pbf.Lls6Temp = int32(res.Lls6Temp)
	pbf.CanFuelLevel = float64(res.CanFuelLevel)
	pbf.ParkingBrakeStatus = int32(res.ParkingBrakeStatus)
	pbf.AccelerationPedalPosition = int32(res.AccelerationPedalPosition) //ver esse
	pbf.EngineOilPressure = 0
	pbf.CoolantTemperature = int32(res.CoolantTemperature)
	pbf.FuelTemperature = 0
	pbf.EngineOilTemperature = 0
	pbf.DailyFuelConsumption = 0
	pbf.EventOfInstantaneousFuelEconomy = int32(res.EventOfInstantaneousFuelEconomy)
	pbf.DailyMileage = int32(res.DailyMileage) //ver esse
	pbf.TotalMileage = float64(res.TotalMileage) * 0.001
	pbf.TotalMileageFilled = float64(res.TotalMileageFilled) * 0.001
	pbf.TotalTimeOfEngineOperation = float64(res.TotalTimeOfEngineOperation)
	pbf.TotalFuelConsumption = float64(res.TotalFuelConsumption)
	pbf.TotalFuelConsumptionHighResolution = 0
	pbf.ServiceBrakePedalPosition = float64(res.ServiceBrakePedalPosition)
	pbf.ClutchPedalPosition = 0
	pbf.CruiseControlStatus = int32(res.CruiseControlStatus)
	pbf.AxlePressure = 0
	pbf.AxleLoad_1 = res.AxleLoad_1
	pbf.AxleLoad_2 = res.AxleLoad_2
	pbf.AxleLoad_3 = res.AxleLoad_3
	pbf.AxleLoad_4 = res.AxleLoad_4
	pbf.AxleLoad_5 = res.AxleLoad_5
	pbf.Adblue = res.Adblue
	pbf.Gear = res.Gear
	pbf.ServiceBrakePedalStatus = int32(res.ServiceBrakePedalStatus)
	pbf.ClutchPedalStatus = int32(res.ClutchPedalStatus)
	pbf.MileageUntilTheNextMaintenance = int32(res.MileageUntilTheNextMaintenance)
	pbf.EngineRunTimeUntilTheNextMaintenance = 0
	pbf.AxleIndex = int32(res.AxleIndex)
	pbf.CanSpeed = int32(res.CanSpeed) //ver esse
	pbf.DoorStatus = int32(res.DoorStatus)
	pbf.SeatbeltStatus = 0
	pbf.DeviceOpen = 0
	pbf.OutputStatus = 0
	pbf.Gpstime = int32(s.Time)
	pbf.IsValid = isValidDate(tm)
	pbf.Id = strconv.Itoa(s.Id)

	return pbf
}

func isValidDate(date time.Time) bool {
	now := time.Now()
	nowMinus24Hours := now.Add(-(time.Hour * 24))
	nowPlus24Hours := now.Add(time.Hour * 24)

	return date.After(nowMinus24Hours) && date.Before(nowPlus24Hours)
}

func GetConf() conf {
	var c conf
	yamlFile, err := ioutil.ReadFile("/etc/v2-collector.yaml")
	if err != nil {
		log.Error("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		log.Error("Unmarshal: %v", err)
	}

	return c
}
