package collector

import "time"

type LeverageData struct {
	ID                                 int
	Time                               time.Time
	Latitude                           float64
	Longitude                          float64
	TotalMileageFilled                 float64
	VirtualOdometer                    float64
	Mileage                            float64
	CanSpeed                           float64
	GpsSpeed                           float64
	GpsStatus                          int
	Height                             int
	Rpm                                float64
	AccelerationPedalPosition          float64
	CruiseControlStatus                int
	TotalFuelConsumptionHighResolution float64
}

type tripTelemetryProfile struct {
	VehicleID              int     `json:"vehicle_id"`
	MaxSpeed               float64 `json:"maxspeed"`
	DistanceMethod         string  `json:"distance_method"`
	LeverageSpeedPeak      float64 `json:"leverage_speed_peak"`
	UseVirtualOdometer     bool    `json:"use_virtual_odometer"`
	NeutralRangeEnd        int
	WhiteRangeEnd          int
	ExtraEconomyRangeStart int
	ExtraEconomyRangeEnd   int
	PowerRangeStart        int
	HasEcoRoll             bool
	HasDafEcoRoll          bool
}

type Storage struct {
	Id       int     `json:"id"`
	UnitType string  `json:"unit_type"`
	UnitId   string  `json:"unit_id"`
	Time     int     `json:"time"`
	Lat      float64 `json:"lat"`
	Lon      float64 `json:"lon"`
	Speed    int     `json:"speed"`
	Course   int     `json:"course"`
	Height   int     `json:"height"`
	Sats     int     `json:"sats"`
	Params   string  `json:"params"`
	ObjectId int     `json:"object_id"`
}

type OmnicommData struct {
	IgnitionKey                          int     `json:"ign_key"`
	BateryLife                           int     `json:"bat"`
	GsmStatus                            int     `json:"gsm_con"`
	GpsStatus                            int     `json:"gps_acc"`
	Voltage                              float64 `json:"uboard"`
	Mileage                              float64 `json:"mileage"`
	MileageVirt                          float64 `json:"mileage"`
	Acceleration                         int     `json:"a_total"`
	Input1                               int     `json:"in1"`
	Input2                               int     `json:"in2"`
	Rpm                                  int     `json:"rpm"`
	Lls1Value                            int     `json:"lls1_value"`
	Lls1Status                           int     `json:"lls1_flags"`
	Lls1Temp                             int     `json:"lls1_temp"`
	Lls2Value                            int     `json:"lls2_value"`
	Lls2Status                           int     `json:"lls2_flags"`
	Lls2Temp                             int     `json:"lls2_temp"`
	Lls3Value                            int     `json:"lls3_value"`
	Lls3Status                           int     `json:"lls3_flags"`
	Lls3Temp                             int     `json:"lls3_temp"`
	Lls4Value                            int     `json:"lls4_value"`
	Lls4Status                           int     `json:"lls4_flags"`
	Lls4Temp                             int     `json:"lls4_temp"`
	Lls5Value                            int     `json:"lls5_value"`
	Lls5Status                           int     `json:"lls5_flags"`
	Lls5Temp                             int     `json:"lls5_temp"`
	Lls6Value                            int     `json:"lls6_value"`
	Lls6Status                           int     `json:"lls6_flags"`
	lls6_temp                            int     `json:"lls6_temp"`
	ParkingBrakeStatus                   int     `json:"spn70"`
	AccelerationPedalPosition            int     `json:"spn91"`
	EngineOilPressure                    int     `json:"spn100"`
	CoolantTemperature                   int     `json:"spn110"`
	FuelTemperature                      int     `json:"spn174"`
	EngineOilTemperature                 int     `json:"spn175"`
	DailyFuelConsumption                 int     `json:"spn182"`
	EventOfInstantaneousFuelEconomy      int     `json:"spn184"`
	DailyMileage                         int     `json:"spn244"`
	TotalMileage                         int     `json:"spn245"`
	TotalMileageFilled                   int     `json:"GapFill"`
	TotalTimeOfEngineOperation           int     `json:"spn247"`
	TotalFuelConsumption                 int     `json:"spn250"`
	ServiceBrakePedalPosition            int     `json:"spn521"`
	ClutchPedalPosition                  int     `json:"spn522"`
	CruiseControlStatus                  int     `json:"spn527"`
	AxlePressure                         int     `json:"spn582"`
	ServiceBrakePedalStatus              int     `json:"spn597"`
	ClutchPedalStatus                    int     `json:"spn598"`
	MileageUntilTheNextMaintenance       int     `json:"spn914"`
	EngineRunTimeUntilTheNextMaintenance int     `json:"spn916"`
	AxleIndex                            int     `json:"spn928"`
	CanSpeed                             int     `json:"spn1624"`
	DoorStatus                           int     `json:"spn1821"`
	SeatbeltStatus                       int     `json:"spn1856"`
	DeviceOpen                           int     `json:"dev_open"`
	OutputStatus                         int     `json:"out_stat"`
}

type RuptellaData struct {
	IgnitionKey                          int     `json:"ign"`
	BateryLife                           int     `json:"bat_v"`
	GsmStatus                            int     `json:"gsm_1v1"`
	GpsStatus                            int     `json:"gps_status"`
	Voltage                              int     `json:"pwr"`
	Mileage                              float64 `json:"virt_odo_diff"`
	MileageVirt                          float64 `json:"virt_odo"`
	Acceleration                         float64 `json:"not_set"`
	Input0                               int     `json:"not_set"`
	Input1                               int     `json:"ot_din1"`
	Input2                               int     `json:"ot_din2"`
	Input3                               int     `json:"ot_din3"`
	Input4                               int     `json:"ot_din4"`
	Rpm                                  float64 `json:"can_eng_spd"`
	Lls1Value                            int     `json:"dfs_c1"`
	Lls1Status                           int     `json:"dfs_c1_status"`
	Lls1Temp                             int     `json:"can_fst_c1"`
	Lls2Value                            int     `json:"dfs_c2"`
	Lls2Status                           int     `json:"dfs_c2_status"`
	Lls2Temp                             int     `json:"can_fst_c2"`
	Lls3Value                            int     `json:"dfs_c3"`
	Lls3Status                           int     `json:"dfs_c3_status"`
	Lls3Temp                             int     `json:"can_fst_c3"`
	Lls4Value                            int     `json:"dfs_a1"`
	Lls4Status                           int     `json:"dfs_a1_status"`
	Lls4Temp                             int     `json:"dfs_a1_t"`
	Lls5Value                            int     `json:"dfs_b1"`
	Lls5Status                           int     `json:"dfs_b1_status"`
	Lls5Temp                             int     `json:"dfs_b1_t"`
	Lls6Value                            float64 `json:"can_fll1"`
	Lls6Status                           int     `json:"can_fll1_status"`
	Lls6Temp                             int     `json:"can_fst_c6"`
	CanFuelLevel                         int     `json:"can_fll1"`
	ParkingBrakeStatus                   int     `json:"car_park"`
	AccelerationPedalPosition            int     `json:"can_acc_pp1"`
	EngineOilPressure                    int     `json:"not_set"`
	CoolantTemperature                   int     `json:"can_eng_cool_t"`
	FuelTemperature                      int     `json:"not_set"`
	EngineOilTemperature                 int     `json:"not_set"`
	DailyFuelConsumption                 int     `json:"not_set"`
	EventOfInstantaneousFuelEconomy      int     `json:"can_f_econ"`
	DailyMileage                         int     `json:"not_set"`
	TotalMileage                         float64 `json:"can_dist_total"`
	TotalMileageFilled                   float64 `json:"can_dist_total"`
	TotalTimeOfEngineOperation           float64 `json:"can_eng_hrs"`
	TotalFuelConsumption                 float64 `json:"can_eng_tfu": `
	TotalFuelConsumptionHighResolution   float64 `json:"can_f_total"`
	ServiceBrakePedalPosition            float64 `json:"can_brake_pp"`
	ClutchPedalPosition                  int     `json:"not_set"`
	CruiseControlStatus                  int     `json:"can_cc"`
	AxleLoad_1                           float64 `json:"can_ax1_w"`
	AxleLoad_2                           float64 `json:"can_ax2_w"`
	AxleLoad_3                           float64 `json:"can_ax3_w"`
	AxleLoad_4                           float64 `json:"can_ax4_w"`
	AxleLoad_5                           float64 `json:"can_ax5_w"`
	Adblue                               float64 `json:"can_adblue"`
	Gear                                 float64 `json:"can_gear_cur"`
	ServiceBrakePedalStatus              int     `json:"can_brake"`
	ClutchPedalStatus                    int     `json:"can_clutch"`
	MileageUntilTheNextMaintenance       int     `json:"can_serv_dist"`
	EngineRunTimeUntilTheNextMaintenance int     `json:"not_set"`
	AxleIndex                            int     `json:"can_axl_loc"`
	CanSpeed                             int     `json:"can_spd_tacho"`
	DoorStatus                           int     `json:"can_door_pos"`
	SeatbeltStatus                       int     `json:"not_set"`
	DeviceOpen                           int     `json:"not_set"`
	OutputStatus                         int     `json:"not_set"`
}

type conf struct {
	Collect  CollectStct  `yaml:"collect"`
	Server   ServerStct   `yaml:"server"`
	DataBase DataBaseStct `yaml:"database"`
}

type CollectStct struct {
	TimeTick int `yaml:"timetick"`
}

type ServerStct struct {
	Telemetry string `yaml:"telemetry"`
}

type DataBaseStct struct {
	Access string `yaml:"access"`
}
