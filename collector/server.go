package collector

import (
	"context"
	"time"
	"v2-collector/pb"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func sendToTelemetry(record pb.AddRequest) {

	cf := GetConf()

	conn, err := grpc.Dial(cf.Server.Telemetry, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Error(err)
	}
	defer conn.Close()
	c := pb.NewTelemetryClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	_, err = c.Add(ctx, &record)
	if err != nil {
		log.Error(err)

	}

}
