package main

import (
	"time"
	"v2-collector/collector"
)

func main() {

	c := collector.GetConf()
	d := time.Duration(c.Collect.TimeTick * (1000000000 * 60))
	collector.Collect()
	for range time.Tick(d) {

		go collector.Collect()

	}
}
