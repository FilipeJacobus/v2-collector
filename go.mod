module v2-collector

go 1.15

require (
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/prometheus/common v0.20.0
	github.com/sirupsen/logrus v1.6.0
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v2 v2.3.0
	mellium.im/sasl v0.2.1 // indirect
)
